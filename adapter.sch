EESchema Schematic File Version 2
LIBS:microchip-modules
LIBS:nrf24l01p_smd
LIBS:hopeRF
LIBS:sma
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:adapter-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "RFM12B / RFM69CW breakout board"
Date "22 Dec 2014"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RFM12B U1
U 1 1 54981C20
P 3800 4350
F 0 "U1" H 3100 4850 60  0000 C CNN
F 1 "RFM12B" H 3250 3950 60  0000 C CNN
F 2 "hopeRF:RFM12B" H 3800 4350 60  0000 C CNN
F 3 "" H 3800 4350 60  0000 C CNN
	1    3800 4350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X07 P3
U 1 1 54989C72
P 5600 4300
F 0 "P3" H 5600 4700 50  0000 C CNN
F 1 "CONN_01X07" V 5700 4300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07" H 5600 4300 60  0001 C CNN
F 3 "" H 5600 4300 60  0000 C CNN
	1    5600 4300
	1    0    0    1   
$EndComp
$Comp
L CONN_01X07 P1
U 1 1 54989D32
P 1900 4300
F 0 "P1" H 1900 4700 50  0000 C CNN
F 1 "CONN_01X07" V 2000 4300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07" H 1900 4300 60  0001 C CNN
F 3 "" H 1900 4300 60  0000 C CNN
	1    1900 4300
	-1   0    0    1   
$EndComp
Text Label 4700 4500 0    60   ~ 0
VDD
Text Label 4700 4400 0    60   ~ 0
GND
Text Label 2850 4600 2    60   ~ 0
GND
$Comp
L PWR_FLAG #FLG01
U 1 1 5498A738
P 5850 7500
F 0 "#FLG01" H 5850 7595 30  0001 C CNN
F 1 "PWR_FLAG" H 5850 7680 30  0000 C CNN
F 2 "" H 5850 7500 60  0000 C CNN
F 3 "" H 5850 7500 60  0000 C CNN
	1    5850 7500
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5498A7FB
P 6300 7500
F 0 "#FLG02" H 6300 7595 30  0001 C CNN
F 1 "PWR_FLAG" H 6300 7680 30  0000 C CNN
F 2 "" H 6300 7500 60  0000 C CNN
F 3 "" H 6300 7500 60  0000 C CNN
	1    6300 7500
	-1   0    0    1   
$EndComp
Text Label 5350 7500 0    60   ~ 0
GND
Text Label 6750 7500 2    60   ~ 0
VDD
$Comp
L SMA J1
U 1 1 5498AAC3
P 6050 5250
F 0 "J1" H 6175 5565 60  0000 C CNN
F 1 "SMA" H 6240 5490 60  0000 C CNN
F 2 "SMA:SMA" H 6225 5420 50  0001 C CNN
F 3 "" H 6050 5250 60  0000 C CNN
	1    6050 5250
	0    -1   1    0   
$EndComp
Text Label 6400 5300 2    60   ~ 0
GND
Text Notes 2850 5150 0    60   ~ 0
RFM12B abd RFM69CW have same\npinout / functionnalities / SMD footprint
$Comp
L nrf24L01P_smd U2
U 1 1 54997646
P 3800 1550
F 0 "U2" H 3750 1900 60  0000 C CNN
F 1 "nrf24L01P_smd" H 4050 1100 60  0000 C CNN
F 2 "nRF24L01:nRF24L01P_smd" H 3600 1550 60  0001 C CNN
F 3 "" H 3600 1550 60  0000 C CNN
	1    3800 1550
	1    0    0    -1  
$EndComp
Text Label 4700 4100 0    60   ~ 0
SCK
Text Label 4700 4200 0    60   ~ 0
MOSI
Text Label 4700 4000 0    60   ~ 0
SS
Text Label 2850 4000 2    60   ~ 0
MISO
Text Label 2850 4100 2    60   ~ 0
IRQ
Text Notes 7050 6800 0    60   ~ 0
This breakout board is not intended to hold at the same time all\nof the RF modules ... you ought to choose only one of them ;)
Text Notes 2650 3700 0    100  ~ 0
RFM12B / RFM69CW modules
Text Notes 2250 900  0    100  ~ 0
nRF24L01 / nRF24L01+ modules
Text Label 2850 4500 2    60   ~ 0
RST
$Comp
L LD1117S33CTR U3
U 1 1 54A2BE31
P 3750 6550
F 0 "U3" H 3750 6800 40  0000 C CNN
F 1 "LD1117S33CTR" H 3750 6750 40  0000 C CNN
F 2 "SMD_Packages:SOT-223" H 3750 6650 40  0000 C CNN
F 3 "" H 3750 6550 60  0000 C CNN
	1    3750 6550
	1    0    0    -1  
$EndComp
Text Label 5150 4600 0    60   ~ 0
RAW
$Comp
L GND #PWR03
U 1 1 54A2C19C
P 3750 7200
F 0 "#PWR03" H 3750 6950 60  0001 C CNN
F 1 "GND" H 3750 7050 60  0000 C CNN
F 2 "" H 3750 7200 60  0000 C CNN
F 3 "" H 3750 7200 60  0000 C CNN
	1    3750 7200
	1    0    0    -1  
$EndComp
Text Label 3000 6500 0    60   ~ 0
RAW
Text Label 4600 6500 2    60   ~ 0
VDD
$Comp
L CAPAPOL C1
U 1 1 54A2C29D
P 4300 6750
F 0 "C1" H 4350 6850 40  0000 L CNN
F 1 "22µF" H 4350 6650 40  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeS_EIA-3216" H 4400 6600 30  0001 C CNN
F 3 "" H 4300 6750 300 0000 C CNN
	1    4300 6750
	1    0    0    -1  
$EndComp
Text Notes 2950 6200 0    60   ~ 0
Apply power through VDD with 3.3v only\nOR apply 4.75v <= RAW <= 12v
$Comp
L CONN_01X08 P2
U 1 1 54A2DA80
P 2450 1600
F 0 "P2" H 2450 2050 50  0000 C CNN
F 1 "CONN_01X08" V 2550 1600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 2450 1600 60  0001 C CNN
F 3 "" H 2450 1600 60  0000 C CNN
	1    2450 1600
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 4500 5400 4500
Wire Wire Line
	4700 4400 5400 4400
Wire Wire Line
	4700 4300 5400 4300
Wire Wire Line
	4700 4200 5400 4200
Wire Wire Line
	4700 4100 5400 4100
Wire Wire Line
	4700 4000 5150 4000
Wire Wire Line
	5150 4000 5400 4000
Wire Wire Line
	2100 4000 2850 4000
Wire Wire Line
	2100 4600 2850 4600
Wire Wire Line
	2100 4500 2850 4500
Wire Wire Line
	2100 4400 2850 4400
Wire Wire Line
	2100 4300 2850 4300
Wire Wire Line
	2100 4200 2850 4200
Wire Wire Line
	2100 4100 2850 4100
Wire Wire Line
	4700 4600 4900 4600
Wire Wire Line
	6750 7500 6300 7500
Wire Wire Line
	5350 7500 5850 7500
Wire Wire Line
	5950 5150 5950 5200
Wire Wire Line
	5950 5200 5950 5250
Wire Wire Line
	5950 5250 5950 5300
Connection ~ 5950 5250
Connection ~ 5950 5200
Wire Wire Line
	5950 5300 6400 5300
Wire Wire Line
	5150 4600 5400 4600
Wire Wire Line
	3750 6800 3750 7100
Wire Wire Line
	3750 7100 3750 7200
Wire Wire Line
	3000 6500 3350 6500
Wire Wire Line
	4150 6500 4300 6500
Wire Wire Line
	4300 6500 4600 6500
Wire Wire Line
	4300 6550 4300 6500
Connection ~ 4300 6500
Wire Wire Line
	4300 6950 4300 7100
Wire Wire Line
	4300 7100 3750 7100
Connection ~ 3750 7100
Wire Wire Line
	2650 1250 3000 1250
Wire Wire Line
	2650 1350 3000 1350
Wire Wire Line
	2650 1450 3000 1450
Wire Wire Line
	2650 1550 3000 1550
Wire Wire Line
	2650 1650 3000 1650
Wire Wire Line
	2650 1750 3000 1750
Wire Wire Line
	3000 1850 2650 1850
Wire Wire Line
	2650 1950 3000 1950
$Comp
L MRF24J40_smd U4
U 1 1 54A317CC
P 8350 1800
F 0 "U4" H 8350 2550 60  0000 C CNN
F 1 "MRF24J40_smd" H 8350 1400 60  0000 C CNN
F 2 "Microchip-modules:MRF24J40MA_smd" H 8350 2100 60  0001 C CNN
F 3 "" H 8350 2100 60  0000 C CNN
	1    8350 1800
	1    0    0    -1  
$EndComp
Text Notes 7250 900  0    100  ~ 0
MRF24J40 802.15.4 transceiver
$Comp
L CONN_01X06 P4
U 1 1 54A31938
P 7250 1850
F 0 "P4" H 7250 2200 50  0000 C CNN
F 1 "CONN_01X06" V 7350 1850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 7250 1850 60  0001 C CNN
F 3 "" H 7250 1850 60  0000 C CNN
	1    7250 1850
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X06 P5
U 1 1 54A319F2
P 9450 1850
F 0 "P5" H 9450 2200 50  0000 C CNN
F 1 "CONN_01X06" V 9550 1850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 9450 1850 60  0001 C CNN
F 3 "" H 9450 1850 60  0000 C CNN
	1    9450 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1600 7800 1600
Wire Wire Line
	7450 1700 7800 1700
Wire Wire Line
	7450 1800 7800 1800
Wire Wire Line
	7450 1900 7800 1900
Wire Wire Line
	7450 2000 7800 2000
Wire Wire Line
	7450 2100 7800 2100
Wire Wire Line
	8900 1600 9250 1600
Wire Wire Line
	8900 1700 9250 1700
Wire Wire Line
	8900 1800 9250 1800
Wire Wire Line
	8900 2000 9250 2000
Wire Wire Line
	8900 2100 9250 2100
NoConn ~ 9250 1900
Text Label 7450 1600 0    60   ~ 0
GNDU4
Text Label 8900 1600 0    60   ~ 0
GNDU4
Text Label 8900 1700 0    60   ~ 0
GNDU4
$Comp
L R R1
U 1 1 54A3D11B
P 5150 3750
F 0 "R1" V 5230 3750 40  0000 C CNN
F 1 "100K" V 5157 3751 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5080 3750 30  0001 C CNN
F 3 "" H 5150 3750 30  0000 C CNN
	1    5150 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 3900 5150 4000
Connection ~ 5150 4000
Wire Wire Line
	5150 3600 5150 3250
Wire Wire Line
	5150 3250 5550 3250
Text Label 5550 3250 2    60   ~ 0
VDD
Text Notes 5300 3700 0    60   ~ 0
This pullup resistor is to avoid trouble\non first time programming of an AVR\nwhile connected to this RFMxx board.
$Comp
L R R2
U 1 1 54A47111
P 5300 5100
F 0 "R2" V 5380 5100 40  0000 C CNN
F 1 "0R" V 5307 5101 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5230 5100 30  0001 C CNN
F 3 "" H 5300 5100 30  0000 C CNN
	1    5300 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 5100 5750 5100
Wire Wire Line
	4900 4600 4900 4900
Wire Wire Line
	4900 4900 4900 5100
Wire Wire Line
	4900 5100 5150 5100
$Comp
L CONN_01X01 P6
U 1 1 54A47377
P 5250 4900
F 0 "P6" H 5250 5000 50  0000 C CNN
F 1 "CONN_01X01" H 5300 4800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 5250 4900 60  0001 C CNN
F 3 "" H 5250 4900 60  0000 C CNN
	1    5250 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4900 4900 4900
Connection ~ 4900 4900
Text Notes 5700 5000 0    60   ~ 0
Antenna wire in P6 or add 0ohm R2 to\nconnect SMA antenna to SMA connector.
Connection ~ 5150 3600
Connection ~ 5150 3900
Connection ~ 5450 5100
Connection ~ 5150 5100
$EndSCHEMATC
