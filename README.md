# KICAD RFM12B / RFM69CW break-out boards #

This project features multiple break-out boards for various low-cost RF modules:

- RFM12B / RFM69CW adapter
- nRF24L01+ adapter
- MRF24J40 adapter

## Issue detected after PCB processing (January 29th, 2015) ##
A minor mistake has been detected in both the nRF24L01+ adapter and the MRF24J40 adapter. For both of them, the vias on the ground plane does not electrically connect both faces. Corrections will be provided later.

** Clone **
```
git clone --recursive git@bitbucket.org:fthiebolt/kicad-rpi.git
cd kicad-rfboards
git submodule foreach 'git checkout master'
```

## PCBs ##
Regarding the PCBs, there are two versions:

- adapter.kicad-pcb --> pcb with those 3 adapters
- adapter-append.kicad-pcb --> 10cm x 10cm pcb featuring 4 times the previous pcb

![kicad-rfboards-adapter-append-pcb_jan15.png](https://bitbucket.org/repo/ByGpRE/images/937846640-kicad-rfboards-adapter-append-pcb_jan15.png)

### KICAD Installer script ###

My modified script will install the recommended kikad bzr rev. **5054** (august 14).

One annoying point with Kicad is that when you compile it with wxGTK3 then, when drawing some schematic, the component attached to the mouse disappear ?!?! (and i even experienced this behaviour on both CentOS7 and FC21).

As a work-around, i modified the `kicad-install.sh` script to force it to use wxGTK along with some tweaking.